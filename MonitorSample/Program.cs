﻿using MonitorSample.Monitors.DiskSpace;
using MonitorSample.Monitors.ServciesPing;
using System;

namespace MonitorSample
{
    class Program
    {
        private static readonly IDiskSpaceMonitor _diskMonitor;
        private static readonly IPingService _pingMonitor;

        static void Main(string[] args)
        {
            // shedule recurrent job for hangfire 
            _diskMonitor.Check();
            _pingMonitor.PingServices();
        }
    }
}
