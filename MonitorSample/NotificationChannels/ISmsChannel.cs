﻿using System.Threading.Tasks;

namespace MonitorSample.NotificationChannels
{
    public interface ISmsChannel
    {
        Task SendSms(string[] phones, string text);
    }
}
