﻿using System.Threading.Tasks;

namespace MonitorSample.NotificationChannels
{
    public interface ITelegramChannel
    {
        Task AddMessage(string message);
    }
}
