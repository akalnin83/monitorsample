﻿using MonitorSample.Monitors.DiskSpace.Models;
using MonitorSample.NotificationChannels;
using System.Threading.Tasks;

namespace MonitorSample.Monitors.DiskSpace
{
    public class DiskSpaceMonitor : IDiskSpaceMonitor
    {
        private readonly IDiskSpaceService _service;
        private readonly IDiskSpaceLogger _logger;
        private readonly ISmsChannel _sms;
        private readonly ITelegramChannel _telegram;
        private const float _minFreeSpaceGb = 3f;

        public Task Check()
        {
            var currState = _service.GetState();

            _logger.Log(currState);
            return NotifyIfNeeded(currState);
        }

        private async Task NotifyIfNeeded(DiskSpaceState currState)
        {
            if (currState.DiskCFreeSpace < _minFreeSpaceGb)
            {
                var text = $"not enough disk C free space, current amount = {currState.DiskCFreeSpace}";
                await _sms.SendSms(new string[] { "121212", "232323" }, text);
                await _telegram.AddMessage(text);
            }

            if (currState.DiskDFreeSpace < _minFreeSpaceGb)
            {
                var text = $"not enough disk D free space, current amount = {currState.DiskDFreeSpace}";
                await _telegram.AddMessage(text);
            }
        }
    }
}
