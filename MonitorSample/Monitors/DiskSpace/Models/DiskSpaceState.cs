﻿namespace MonitorSample.Monitors.DiskSpace.Models
{
    public class DiskSpanceState
    {
        public float DiskCFreeSpace { get; set; }

        public float DiskDFreeSpace { get; set; }
    }
}
