﻿using MonitorSample.Monitors.DiskSpace.Models;

namespace MonitorSample.Monitors.DiskSpace
{
    public interface IDiskSpaceService
    {
        public DiskSpaceState GetState();
    }
}
