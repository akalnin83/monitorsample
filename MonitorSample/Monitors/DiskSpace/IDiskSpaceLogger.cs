﻿using MonitorSample.Monitors.DiskSpace.Models;

namespace MonitorSample.Monitors.DiskSpace
{
    public interface IDiskSpaceLogger
    {
        void Log(DiskSpaceState state);
    }
}
