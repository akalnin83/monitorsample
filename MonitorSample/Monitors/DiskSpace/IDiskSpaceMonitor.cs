﻿using System.Threading.Tasks;

namespace MonitorSample.Monitors.DiskSpace
{
    public interface IDiskSpaceMonitor
    {
        Task Check();
    }
}