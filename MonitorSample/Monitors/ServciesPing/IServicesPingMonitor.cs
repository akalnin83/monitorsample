﻿using System.Threading.Tasks;

namespace MonitorSample.Monitors.ServciesPing
{
    public interface IServicesPingMonitor
    {
        Task Check();
    }
}