﻿using MonitorSample.Monitors.ServciesPing.Models;
using System.Threading.Tasks;

namespace MonitorSample.Monitors.ServciesPing
{
    public interface IPingService
    {
        public Task<PingResult> PingServices();
    }
}
