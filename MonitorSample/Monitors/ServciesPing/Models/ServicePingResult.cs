﻿namespace MonitorSample.Monitors.ServciesPing.Models
{
    public class ServicePingResult
    {
        public string Url { get; set; }

        public string HttpCode { get; set; }

        public float ResponceTime { get; set; }
    }
}
