﻿using MonitorSample.Monitors.ServciesPing.Models;
using MonitorSample.NotificationChannels;
using System.Linq;
using System.Threading.Tasks;

namespace MonitorSample.Monitors.ServciesPing
{
    public class ServicesPingMonitor : IServicesPingMonitor
    {
        private readonly IPingService _pingService;
        private readonly ITelegramChannel _telegram;

        public async Task Check()
        {
            var currState = await _pingService.PingServices();
            await NotifyIfNeeded(currState);
        }

        private async Task NotifyIfNeeded(PingResult currState)
        {
            var serverError = currState.Results.Where(u => u.HttpCode == "500");
            if (serverError.Any())
            {
                foreach (var service in serverError)
                {
                    var text = $"service {service.Url} answered with 500 error";
                    await _telegram.AddMessage(text);
                }
            }

            // other checks
        }
    }
}
